# from stem import Signal
# from stem.control import Controller

# with Controller.from_port(port = 9051) as controller:
#   controller.authenticate()
#   controller.signal(Signal.NEWNYM)

# from selenium import webdriver
# from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
# from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
# driver = webdriver.Firefox()



import socks  # SocksiPy module
import socket
import requests

from stem import Signal
from stem.control import Controller

with Controller.from_port(port = 9151) as controller:
    controller.authenticate()
    controller.signal(Signal.NEWNYM)

SOCKS_PORT = 9150

# Set socks proxy and wrap the urllib module

socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, '127.0.0.1', SOCKS_PORT)
socket.socket = socks.socksocket

def getaddrinfo(*args):
  return [(socket.AF_INET, socket.SOCK_STREAM, 6, '', (args[0], args[1]))]

socket.getaddrinfo = getaddrinfo

url = "http://www.whatsmyip.org"
# driver.get("https://www.google.com/search?q=whats+my+ip")
# print requests.get(url).text


# profile = webdriver.FirefoxProfile()
# profile.set_preference('network.proxy.type', 1)
# profile.set_preference('network.proxy.socks', '127.0.0.1')
# profile.set_preference('network.proxy.socks_port', 9150)
# profile.set_preference("network.proxy.socks_version", 5)
# profile.update_preferences()


# 222.76.49.24

# https://tor.stackexchange.com/questions/13252/changing-identity-using-tor-and-stem/15078