import os, re, random, os, time, datetime, logging, shutil, sys
program_data_dir = os.environ['USERPROFILE']+"\\Documents\\TrafficGenLTD"
try:
    import selenium
except:
    print("IMPORTANT:")
    print("Upon running you must ALLOW the execution of program through firewall and your Anti-Virus!")
    print("In order to modify the list of URLs and parameters, edit the traffic2sites.xlsx file in {}".format(program_data_dir))
    print("First time running. Installing necessary components..")
    print("After installation of necessary components, program will exit and you'll have to start it again.")
    raw_input("\n\nPress ENTER to continue...")

    os.environ["PATH"] += os.pathsep + sys.exec_prefix
    os.system('Instaler.py')
    raw_input("\n\nInstallation complete. Please start the program again. Press ENTER to exit...")
    exit(1)

_ = os.system('cls') 
print("IMPORTANT:")
print("Upon running you must ALLOW the execution of program through firewall and your Anti-Virus!")
print("In order to modify the list of URLs and parameters, edit the traffic2sites.xlsx file in {}".format(program_data_dir))
print("You can minimize both Browser's windows!")

from pywinauto import Desktop

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from subprocess import Popen
from os import getcwd
from openpyxl import load_workbook
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import Tkinter, tkFileDialog, requests

# Yeah let's make it 20 min per website
# At least 10 pages on each website before changing ip
#  let's aim to increase new IP visits to 75 per website
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
file_handler = logging.FileHandler(program_data_dir + '\\Traffic.log')
#file_handler.setLevel(logging.ERROR)
#logger.exception('Tried to divide by zero')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(formatter)
logger.addHandler(consoleHandler)

def scrollDown(actions):
    actions.send_keys(Keys.PAGE_DOWN).perform()
    time.sleep(1)

def scrollUp(actions):
    actions.send_keys(Keys.PAGE_UP).perform()
    time.sleep(1)

def page_has_loaded_id(driver):
    try:
        new_page = driver.find_element_by_tag_name('html')
        return True
    except NoSuchElementException:
        return False

def adsAndMute(driver):
    try:
        driver.find_element_by_xpath('//*[@title="Mute"]').click()
    except NoSuchElementException:
        pass

    time.sleep(3)
    try:
        driver.find_element_by_class_name('videoAdUiSkipButton videoAdUiAction videoAdUiFixedPaddingSkipButton').click()
    except NoSuchElementException:
        pass
    try:
        driver.find_element_by_xpath('//*[@title="Mute"]').click()
    except NoSuchElementException:
        pass

def MinimizeAboutTorWindow():
    counter = 0
    while(counter <2):
        windows = Desktop(backend='uia').windows()
        for w in windows:
            if str(w.element_info).find('Tor Browser') > -1:
                w.minimize()
                counter += 1
    ##        if 'About Tor' in str(w.element_info):
    ##            w.minimize()
    ##        if 'TrafficGenTor' in str(w.element_info):
    ##            w.minimize()
def initDriver(minimizedScreen):
    #path.txt must be used because of the Initialize.bat!!!
    binary = FirefoxBinary('{}\\Browser\\firefox.exe'.format(open('path.txt', 'r').read().strip('\n')))
    # binary = FirefoxBinary(program_data_dir+"\\Tor Browser\\Browser\\firefox.exe")

    # Popen('initialize.bat', cwd=getcwd())
    # caps = DesiredCapabilities().FIREFOX
    # caps["pageLoadStrategy"] = "none"  #  interactive
    # firefox_capabilities = DesiredCapabilities.FIREFOX
    # firefox_capabilities['marionette'] = True
    # firefox_capabilities['firefox_profile'] = profile.encoded



    profile = webdriver.FirefoxProfile()  
    profile.set_preference("media.volume_scale", "0")






    # profile.add_extension("uBlock.xpi")
    # profile.set_preference('network.proxy.type', 1)
    # profile.set_preference('network.proxy.socks', '127.0.0.1')
    # profile.set_preference('network.proxy.socks_port', 9051)
    # profile.set_preference('network.cookie.lifetimePolicy', 2)
    # profile.set_preference('network.dns.disablePrefetch', True)
    # profile.set_preference('network.http.sendRefererHeader', 0)
    # # profile.set_preference('privacy.clearOnShutdown.offlineApps', True)
    # # profile.set_preference('privacy.clearOnShutdown.passwords', True)
    # profile.set_preference('privacy.clearOnShutdown.siteSettings', True)
    # # profile.set_preference('privacy.sanitize.sanitizeOnShutdown', True)
    # profile.set_preference('signon.rememberSignons', False)
    # profile.set_preference('places.history.enabled', False)
    # profile.set_preference('javascript.enabled', True)
    #--- above was commented out--
    driver = webdriver.Firefox(firefox_binary=binary,firefox_profile = profile, log_path = program_data_dir + r'\\geckodriver.log')
    if minimizedScreen == "yes":
        MinimizeAboutTorWindow()
        return driver
    return driver

def xpath_soup(element):
    import itertools
    """
    Generate xpath from BeautifulSoup4 element
    :param element: BeautifulSoup4 element.
    :type element: bs4.element.Tag or bs4.element.NavigableString
    :return: xpath as string
    :rtype: str
    
    Usage:
    
    >>> import bs4
    >>> html = (
    ...     '<html><head><title>title</title></head>'
    ...     '<body><p>p <i>1</i></p><p>p <i>2</i></p></body></html>'
    ...     )
    >>> soup = bs4.BeautifulSoup(html, 'html.parser')
    >>> xpath_soup(soup.html.body.p.i)
    '/html/body/p[1]/i'
    """
    """
    Generate xpath of soup element
    :param element: bs4 text or node
    :return: xpath as string
    """
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:
        """
        @type parent: bs4.element.Tag
        """
        previous = itertools.islice(parent.children, 0, parent.contents.index(child))
        xpath_tag = child.name
        xpath_index = sum(1 for i in previous if i.name == xpath_tag) + 1
        components.append(xpath_tag if xpath_index == 1 else '%s[%d]' % (xpath_tag, xpath_index))
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)

def main():
    wb = load_workbook(filename = program_data_dir+'\\traffic2sites.xlsx', read_only = True)
    # Getting Data params
    params = wb[wb.sheetnames[1]] # 'Data' sheet
    # timeOnWebsiteMin = 2
    # timeOnWebsiteMax = 4
    # videoPercent = 5
    # loopMin = 20
    # loopMax = 25
    timeOnWebsiteMin = params['B1'].value
    timeOnWebsiteMax = params['B2'].value
    videoPercentMin = params['B3'].value
    videoPercentMax = params['B4'].value
    loopMin = params['B5'].value
    loopMax = params['B6'].value
    numberOfPagesMin = params['B7'].value
    numberOfPagesMax = params['B8'].value
    minimizedScreen = params['B9'].value


    loops = random.randint(loopMin, loopMax)


    logger.info("Session started on: {}".format(datetime.datetime.now().strftime("%I:%M%p, %B %d, %Y")))
    logger.info("""\n\tParametres:\n\tTime on site (min-max): {}-{} \n\tVideo percentage (min-max):{}-{} 
        Loops (min-max): {}-{} \n\tLoops: {} \n\tNumber of pages (min-max): {}-{}
        \tMinimized Screen: {}""".format(timeOnWebsiteMin, timeOnWebsiteMax,
        videoPercentMin, videoPercentMax, loopMin, loopMax, loops, numberOfPagesMin, numberOfPagesMax, minimizedScreen))

    links = wb[wb.sheetnames[0]]
    maxRow = links.max_row
    urls = []
    for i in range(1,maxRow+1):
        urls.append(links['A'+str(i)].value)
    random.shuffle(urls)

    timeOnWebsite = random.randint(timeOnWebsiteMin, timeOnWebsiteMax)*60
    logger.debug('Number of loops: {}. Time on website in seconds: {}'.format(loops, timeOnWebsite))
    Popen('initialize.bat', cwd=getcwd())
    if not os.path.exists('./screenshot'):
        os.makedirs('./screenshot')
    for i in range(0,loops):
        logger.info('*********Loop {}*********'.format(i))
        for domain in urls[:]:
            if 'http' not in domain:
                domain = 'http://'+domain
            driver = initDriver(minimizedScreen)
            actions = ActionChains(driver)


            # For checking the ip
            # logger.info('\tChecking IP and saving screenshot...')
            # try:
            #     driver.get('https://duckduckgo.com/?q=whats+my+ip&ia=answer')
            #     driver.save_screenshot('/screenshots/Run'+str(i)+'.png')
            # except:
            #     time.sleep(3)
            #     try:
            #         driver.get('https://duckduckgo.com/?q=whats+my+ip&ia=answer')
            #         driver.save_screenshot('/screenshots/Run'+str(i)+'.png')
            #     except:
            #         pass
            logger.info('\tWorking with domain: {}'.format(domain))
            try:
                driver.get(domain)
            except Exception as e:
                # print '\t', e
                logger.exception('\tError while getting domain: {}'.format(domain))
                driver.quit()
                continue

            time.sleep(3)

            if "google" in driver.current_url:
                logger.debug("Hit google's captcha on {} url. Continuing.".format(domain))
                driver.quit()
                time.sleep(3)
                logger.info('\tChanging identity')
                os.system('Identity.py')
                time.sleep(5)
                continue



            if 'youtube' not in domain:
                #driver.find_element_by_tag_name('html').sendKeys(Keys.ESCAPE)
                try:
                    ActionChains(driver).send_keys(Keys.ESCAPE).perform()
                except:
                    pass
                subString = domain+"*"
                blackList = ['.png', '.jpg', '.gif', '.aspx', '.swf']
                soup = BeautifulSoup(driver.page_source,'html.parser')
                dataSet = soup.findAll('a',{'href':re.compile(subString)})
                links = [i.get('href').strip() for i in dataSet if not any(x in i.get('href') for x in blackList)]
                # links_second = [i for i in dataSet if not any(x in i.get('href') for x in blackList)]
                # print '-------------'
                # for i in links_second:
                #     if i.previousSibling:
                #         print xpath_soup(i.previousSibling)
                numberofPages = random.randint(numberOfPagesMin, numberOfPagesMax)
                try:
                    randomDataSet = random.sample(set(links), numberofPages) # VallueError: sample larger than length
                except Exception as e:
                    if len(links)<1:
                        driver.quit()
                        # logger.exception('Something went wrong. Not enough links found. Number of Links: %s. Domain: %s' % (len(links), domain))
                        continue
                    else:
                        # logger.error('Could not find %s number of pages. Instead found only: %s' % (len(links)))
                        randomDataSet = set(links)
                
                timeOnWebsitePerPage = timeOnWebsite/len(randomDataSet)
                logger.info('\tAverage time on website per page in seconds: {}. In minutes: {}. Pages to visit {}.'.format(timeOnWebsitePerPage, timeOnWebsitePerPage/60,numberofPages))
                for link in randomDataSet:
                    logger.debug('\t\tWorking with url:{}, started on:{}.'.format(link, time.strftime("%H:%M:%S", time.localtime())))
                    try:
                        driver.get(link)
                    except:
                        time.sleep(3)
                        try:
                            driver.get(link)
                        except:
                            logger.exception('\t\tError opening: {}, on: {}'.format(link, time.strftime("%H:%M:%S", time.localtime())))
                            continue
                    time.sleep(timeOnWebsitePerPage)
            else:
                soup = BeautifulSoup(driver.page_source,'html.parser')
                lengthOfVideo = 0
                lengthOfVideoTmp = 0
                lengthOfVideoPerc = 0
                lengthOfVideoTmp = soup.find('span',{'class':'ytp-time-duration'}).text.strip().split(':')
                lengthOfVideo = int(lengthOfVideoTmp[0])*60+int(lengthOfVideoTmp[1])
                logger.debug('\t\tLength of the video: {} seconds'.format(lengthOfVideo))
                videoPercent = random.randint(videoPercentMin, videoPercentMax)
                if (lengthOfVideo*videoPercent)/100 > 180:
                    logger.debug('\t\tWaiting: {}'.format((lengthOfVideo*videoPercent)/100))
                    time.sleep((lengthOfVideo*videoPercent)/100)
                else:
                    logger.debug('\t\tVideo duration below two minutes. Waiting 120 seconds') 
                    time.sleep((lengthOfVideo*videoPercent)/100)

            driver.quit()
            time.sleep(3)
            logger.info('\tChanging identity')
            os.system('Identity.py')
            time.sleep(5)   

if __name__ == '__main__':
    main()